<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('amount');
            $table->string('description');
            $table->string('destinationFirstname');
            $table->string('destinationLastname');
            $table->string('destinationNumber');
            $table->string('inquiryDate');
            $table->integer('inquirySequence');
            $table->string('inquiryTime');
            $table->string('message');
            $table->string('paymentNumber');
            $table->string('refCode');
            $table->string('sourceFirstname');
            $table->string('sourceLastname');
            $table->string('sourceNumber');
            $table->string('type');
            $table->string('reasonDescription');
            $table->string('trackId');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
};
