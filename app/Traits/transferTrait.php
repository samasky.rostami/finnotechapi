<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait transferTrait
{
    public function transferInfo($data, $token , $clientId, $trackId)
    {

        try {

            return $response = Http::withToken($token)->withHeaders([
                'Content-Type' => 'application/json',
            ])->post(
                'https://apibeta.finnotech.ir/oak/v2/clients/' . $clientId . '/transferTo?trackId='.$trackId,
                $data
            );
        } catch (\Exception $exception) {
            throw new BadRequestHttpException();
        }


    }
}
