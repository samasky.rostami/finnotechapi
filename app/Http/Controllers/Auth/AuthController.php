<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $validated = (object) $request->validated();

        $user = User::create([
            'nationalId' => $validated->nationalId,
            'firstname' => $validated->firstname,
            'lastname' => $validated->lastname,
            'password' => Hash::make($validated->password),
        ]);

        return response()->json([
            'token' => $user->createToken($validated->nationalId)->plainTextToken
        ]);

    }

    public function login(LoginRequest $request)
    {
        $validated = (object) $request->validated();

        $user = User::firstWhere('nationalId', $validated->nationalId);

        if (!$user || !Hash::check($validated->password, $user->password))
        {
            abort(401, 'User with this credentials could not be found');
        }

        return response()->json([
            'token' => $user->createToken($validated->nationalId)->plainTextToken
        ]);
    }
}
