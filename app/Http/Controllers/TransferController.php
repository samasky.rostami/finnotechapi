<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferRequest;
use App\Models\Account;
use App\Models\Transfer;
use App\Models\User;
use App\Traits\transferTrait;
use Illuminate\Support\Str;

class TransferController extends Controller
{
    use transferTrait;

    private $token = 'f2a1ed52710d4533bde25be6da03b6e3';
    private $clientId = 'ZYDPLLBWSK3MVQJSIYHB1OR2JXCY0X2C5UJ2QAR2MAAIT5Q';


    public function transfer(TransferRequest $request)
    {

        $validated = (object) $request->validated();

        if (!isset($validated->deposit)) {
            $account = Account::where('accounts.accountName', $validated->accountName)->where('user_id', auth()->user()->id)->first();
            if (!$account) {
                return response()->json([
                    "error" => "Account with this name not found"
                ]);
            }
            $validated->deposit = $account->accountNumber;
        }
        unset($validated->accountName);

        $validated = array_merge((array) $validated, [
            'sourceFirstName' => auth()->user()->firstname,
            'sourceLastName' => auth()->user()->lastname,
        ]);


        $apiData = json_decode($this->transferInfo($validated, $this->token, $this->clientId, Str::uuid()));
        if ($apiData->status == "FAILED") {
            return response()->json($apiData->error);
        }


        $transferinfo = Transfer::create([
            'user_id' => auth()->user()->id,
            "trackId" => $apiData->trackId,
            "amount" => $apiData->result->amount,
            "description" => $apiData->result->description,
            "destinationFirstname" => $apiData->result->destinationFirstname,
            "destinationLastname" => $apiData->result->destinationLastname,
            "destinationNumber" => $apiData->result->destinationNumber,
            "inquiryDate" => $apiData->result->inquiryDate,
            "inquirySequence" => $apiData->result->inquirySequence,
            "inquiryTime" => $apiData->result->inquiryTime,
            "message" => $apiData->result->message,
            "paymentNumber" => $apiData->result->paymentNumber,
            "refCode" => $apiData->result->refCode,
            "sourceFirstname" => $apiData->result->sourceFirstname,
            "sourceLastname" => $apiData->result->sourceLastname,
            "sourceNumber" => $apiData->result->sourceNumber,
            "type" => $apiData->result->type,
            "reasonDescription" => $apiData->result->reasonDescription,
        ]);

        return response()->json($transferinfo);
    }
}
