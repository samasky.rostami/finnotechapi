<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddAccountRequest;
use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function add(AddAccountRequest $request)
    {
        $validated = (object) $request->validated();

        $account = Account::where('accounts.accountName', $validated->accountName)->where('user_id', auth()->user()->id )->first();
        if ($account)
        {
            return response()->json([
                "error" => "The account name has already been taken."
            ]);
        }

        $account = Account::create([
            'user_id' => auth()->user()->id,
            'accountName' => $validated->accountName,
            'accountNumber' => $validated->accountNumber
        ]);

        return response()->json($account);
    }
}
