<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|max:500000000',
            'description' => 'required|max:30',
            'destinationFirstname' => 'required|between:2,33',
            'destinationLastname' => 'required|between:2,33',
            'destinationNumber' => 'required|max:26',
            'paymentNumber' => 'max:30',
            'accountName' => 'required_without:deposit|string|max:255|exists:accounts,accountName',
            'deposit' => 'max:26',
            'reasonDescription' => 'numeric|between:1,19',
        ];
    }
}
